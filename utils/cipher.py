#!/usr/bin/env python3

import sys
import base64
import hmac
import hashlib
import binascii
import urllib
import struct
from Crypto.Cipher import AES
import io

en_indent_len = 4
salt_len = 16
salt_hmac_len = 16
iv_len = 16
entire_hmac_len = 32
password = '1234'
en_iteration = 50000
en_keysize = 128
en_bytesize = 8

def b2i(s):
    if type(s) == bytearray:    #python2 and python3
        num_words = (int)(len(s) / 4)
        ret = []
        for i in range(0,num_words):
            val = s[i * 4] << 24 | s[i*4+1] << 16 | s[i*4+2] << 8 | s[i*4+3]
            if( (val&(1<<(32-1))) != 0 ):
                val = val - (1<<32)
            ret.append(val)
        return ret
    elif type(s) == bytes:    #python3
        num_words = (int)(len(s) / 4)
        ret = []
        for i in range(0,num_words):
            val = int.from_bytes(s[i*4:i*4+4], 'big', signed=True)
            ret.append(val)
        return ret

def dump_bytes(tag, s):
    #print('%s: %r' % (tag, b2i(s)))
    print('%s(%d): %r' % (tag, len(s), binascii.hexlify(s)))


def pbkdf2(password, salt, iteration, keylen, hashfn=hashlib.sha256):

    if type(password) != str:
        raise Exception('password should be str')

    if type(salt) != bytes and type(salt) != bytearray:
        raise Exception('salt should be bytes or bytearray')

    def prf(password, msg):
        return hmac.new(key=password, msg=msg, digestmod=hashfn).digest()

    def xor_bytes(bs1, bs2):
        ret = []
        for i in range(0, len(bs1)):
            ret.append(bs1[i] ^ bs2[i])
        return ret

    def pbkdf2_f(password, salt, iteration, block_index):
        U = prf(password, salt + (block_index).to_bytes(4, 'big'))
        T = U
        for i in range(2, iteration + 1):
            U = prf(password, U)
            T = xor_bytes(T, U)
        return T

    digestsize = hashfn().digest_size
    num_blocks = keylen // digestsize
    if keylen % digestsize:
        num_blocks = num_blocks + 1

    T = []
    for i in range(1, num_blocks+1):
        T += pbkdf2_f(password.encode('ascii'), salt, iteration, i)

    # to verify:
    # password: 'password'
    # salt: b'\x12\x34\x56\x78\x12\x34\x56\x78'
    # iteration: 100
    # keylen: 16
    # expected: A3 74 FF 6A 12 28 0F 02 01 62 A6 2A 9B 32 12 AA
    return bytes(T[0:keylen])


#b64_str = ''
#for l in sys.stdin:
#    b64_str += l

b64_str = 'RU5DMCNL07QyJ8U7oyDpsdM6V+H2jIHMRh+G7o005fx3u7elY71dtn0psZ09OjkHzNFGn2x4eTVzmsw6j6wSjLNGUS9WwEELfwmOtEYOmIuixEv6zLlQP8aLecNkQtKD3t+JdRAE3REZawNU/CaV7zdIRHV13PPfddFnjAcGCcwEwHJHdvZjFy0+CkJ/dKzPW2GR0ZowojHTA6Qt5kQIl08ytnjydGix8zmO0nYBG8FRZ7T3si8HRUGLjMJQajhu31DPovPTCuB3IjN4cyQBQmFrE+N5IHLiHymmw7eFyrK4GPNR6IEgh/8Gm0dqo9pMwRirGBJoPiOQ8mJyHdECt0T3gi1JreXBReo7WvI80AcmZX/R/2anwtdEi2STVI30x35jxSyae7lLO+jA/xYpT2nVNdUltLjHmod3gyF7F7qkSD5sB5PHrxeaVVbyjocDovL9Ol16KoySG7uqP1QIYfA2tZsd2LRjHcQvAdQP0PrDP5HdnFDxPKp64/BbNFXKHcOAU64KlQa5jgc1plRNWTf0nMsYV3X/rAizqF1PHo812bCdD4adXWPBWMpR7QlwMphP+iuc0wT/2b/MUr4QyY0O4qoo5367RrxeZ9QMRaLyv7BTFnBL3VxV6g2R2tBkss96qQkK5iHYwu3iV6ZUWxowHQh53CrRVtN7lUEaYSIJ7dWw8UH78jbpELjgJWNwscFkkPU04Eagz8SPVL7QW3P8xZmmlE7l5rovEfET+Fw7Vm/xMq98onOvOBf5jzp5G/GBhweZYhQEqhY3IeaLjL+yENIYBkCUYKmgg/QkUdSEiZopS7KM0UFsay1xn0PttanFSOB/GOlOPYv02gtmnGNjImhQ48SGNECBdsLz38zcsEfrpg2fiKM6XYAPcRX5xmFUcJCeq7tsYobwJviF+wt0B4ZNWv0+OsN1RoR9ykbcPyhfrOdSqZIze6kJ46VAylbx3zg4YkVwsMecJ1HE9kozdzs='

raw = base64.b64decode(b64_str)
cursor = 0


en_indent = raw[cursor:cursor+en_indent_len]
cursor = cursor + en_indent_len
#print(en_indent.decode('ascii'))

salt = raw[cursor:cursor+salt_len]
cursor = cursor + salt_len
dump_bytes('salt', salt)

salt_hmac = raw[cursor:cursor+salt_hmac_len]
cursor = cursor + salt_hmac_len
dump_bytes('salt_hmac', salt_hmac)

iv = raw[cursor:cursor+iv_len]
cursor = cursor + iv_len
dump_bytes('iv', iv)

ciphertext_len = len(raw) - cursor - entire_hmac_len
ciphertext = raw[cursor:cursor+ciphertext_len]
cursor = cursor + ciphertext_len
dump_bytes('ciphertext', ciphertext)

entire_hmac = raw[cursor:]
cursor = cursor + entire_hmac_len
dump_bytes('entire_hmac', entire_hmac)


key_hmac = pbkdf2(password, salt_hmac, en_iteration, en_keysize // en_bytesize)
dump_bytes('key_hmac', key_hmac)

verify_entire_hmac = hmac.new(key=key_hmac, msg=raw[:-entire_hmac_len], digestmod=hashlib.sha256).digest()
dump_bytes('verify_entire_hmac', verify_entire_hmac)
assert entire_hmac == verify_entire_hmac

key = pbkdf2(password, salt, en_iteration, en_keysize // en_bytesize)
dump_bytes('key', key)

cipher = AES.new(key, AES.MODE_CBC, iv)
next_chunk = ''
finished = False
in_file = io.BytesIO(ciphertext)
out_file = io.BytesIO()
while not finished:
    chunk, next_chunk = next_chunk, cipher.decrypt(in_file.read(1024 * AES.block_size))
    if len(next_chunk) == 0:
        padding_length = chunk[-1] # removed ord(...) as unnecessary
        chunk = chunk[:-padding_length]
        finished = True
    out_file.write(bytes(x for x in chunk)) # changed chunk to bytes(...)
print(out_file.getvalue().decode('utf8'))
out_file.close()
