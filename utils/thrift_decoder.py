#!/usr/bin/env python
import sys
import hashlib
import binascii
import time
import re
import hashlib
import codecs

reload(sys)
sys.setdefaultencoding('utf-8')

sys.path.append('../3rd_party/evernote-sdk-python-master/lib/')

import thrift.protocol.TBinaryProtocol as TBinaryProtocol
import thrift.transport.THttpClient as THttpClient
import evernote.edam.userstore.UserStore as UserStore
import evernote.edam.userstore.constants as UserStoreConstants
import evernote.edam.notestore.NoteStore as NoteStore
import evernote.edam.type.ttypes as Types
import evernote.edam.error.ttypes as Errors


from thrift.protocol import TBinaryProtocol
from thrift.transport import TTransport

raw_content = ''
for l in sys.stdin:
    raw_content += l

client = NoteStore.Client(TBinaryProtocol.TBinaryProtocol(TTransport.TMemoryBuffer(raw_content)))
s = ''
while True:
    try:
        s += client.recv_getNoteContent()
    except:
        break
print(s)
