#ifndef _CRYPTO_H_
#define _CRYPTO_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef unsigned char byte_t;

int base64_decode(const char* b64_str, byte_t** ret_buf, size_t* ret_buf_len);

char* base64_encode(const byte_t* data, const size_t nbytes_data);

int hmac_sha256(const byte_t* msg, const size_t nbytes_msg,
                const byte_t* key, size_t nbytes_key,
                byte_t digest[]);

int aes128_cbc_encrypt(const byte_t* plaintext, const size_t nbytes_plaintext,
	                   const byte_t key[], const byte_t iv[],
	                   byte_t** ret_ciphertext, size_t* ret_nbytes_ciphertext);

int aes128_cbc_decrypt(const byte_t* ciphertext, const size_t nbytes_ciphertext,
	                   const byte_t key[], const byte_t iv[],
	                   byte_t** ret_plaintext, size_t* ret_nbytes_plaintext);

byte_t* pbkdf2_hmac_sha256(const char* password,
                           const byte_t* salt, const size_t nbytes_salt,
                           const size_t iteration,
                           const unsigned short keylen);

#ifdef __cplusplus
}
#endif


#endif

