#ifndef _EVERNOTE_H_
#define _EVERNOTE_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "crypto.h"

#define EN_IDENTIFIER_LEN 4UL
#define SALT_LEN 16UL
#define SALT_HMAC_LEN 16UL
#define IV_LEN 16UL
#define ENTIRE_HMAC_LEN 32UL
#define PBKDF2_ITERATION 50000UL
#define KEY_SIZE (128U/8U)

char* evernote_lock_note(const char* password, const char* note, const int is_web);

char* evernote_unlock_note(const char* password, const char* note, const int is_web);

#ifdef __cplusplus
}
#endif


#endif

