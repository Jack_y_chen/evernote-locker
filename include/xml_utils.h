#ifndef _XML_UTILS_H_
#define _XML_UTILS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "libxml/xmlstring.h"
#include "libxml/tree.h"


xmlDocPtr parse_xml(const void* data, const size_t nbytes);

xmlDocPtr parse_html(const void* data, const size_t nbytes);


typedef void (*node_traverse_func_t)(const xmlNodePtr node, void* userdata);

/* walk through nodes under root */
int traverse_nodes(const xmlNodePtr root, const node_traverse_func_t func, void* userdata);


/* find lowest, common ancestor */
xmlNodePtr find_lca(const xmlNodePtr p, const xmlNodePtr q);


/* find the first and the last text node under root */
int find_boundry_text_nodes(const xmlNodePtr root, xmlNodePtr* ret_first_textnode, xmlNodePtr* ret_last_textnode);

#ifdef __cplusplus
}
#endif


#endif

