#ifndef _PARSER_H_
#define _PARSER_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "libxml/xmlstring.h"
#include "libxml/tree.h"


typedef struct {
	size_t offset;
	size_t length;
	xmlNodePtr node;
} text_segment_t;


typedef struct {
	xmlChar* text;
	xmlChar* _p;

	text_segment_t* segments;
	size_t num_segments;
	size_t _i;

} text_col_t;


text_col_t collect_text(const xmlDocPtr doc);

void release_text_col(text_col_t col);


#ifdef __cplusplus
}
#endif


#endif

