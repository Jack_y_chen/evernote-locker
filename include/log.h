#ifndef __LOG_H__
#define __LOG_H__

#include <stdarg.h>


typedef enum {
	LOG_LEVEL_NONE = 0,
	LOG_LEVEL_INFO = 1,
	LOG_LEVEL_WARN = 2,
	LOG_LEVEL_ERR = 3
} log_level_t;



#ifdef __cplusplus
extern "C" {
#endif

#if (__STDC_VERSION__ >= 199901L)

	extern void _log_c99_impl(const log_level_t level, const char* file, const char* function, const unsigned line, const char* format, ...);

	#define log_info(...) _log_c99_impl(LOG_LEVEL_INFO, __FILE__, __FUNCTION__, __LINE__, __VA_ARGS__)
	#define log_warn(...) _log_c99_impl(LOG_LEVEL_WARN, __FILE__, __FUNCTION__, __LINE__, __VA_ARGS__)
	#define log_err(...) _log_c99_impl(LOG_LEVEL_ERR, __FILE__, __FUNCTION__, __LINE__, __VA_ARGS__)

#else

	typedef void(*log_func_t)(const char* format, ...);

	extern log_func_t _log_wrapper(const log_level_t level, const char* file, const char* function, const unsigned int line);

	#define log_info (_log_wrapper(LOG_LEVEL_INFO, __FILE__, __FUNCTION__, __LINE__))
	#define log_warn (_log_wrapper(LOG_LEVEL_WARN, __FILE__, __FUNCTION__, __LINE__))
	#define log_err (_log_wrapper(LOG_LEVEL_ERR, __FILE__, __FUNCTION__, __LINE__))

#endif

#ifdef __cplusplus
}
#endif

#endif

