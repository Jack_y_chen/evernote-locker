#ifndef _MATCHER_H_
#define _MATCHER_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
	size_t start;
	size_t end;
} match_t;

int match_credit_card(const char* text, match_t mathes[], const size_t nmatches);

#ifdef __cplusplus
}
#endif


#endif

