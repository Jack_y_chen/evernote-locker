#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <errno.h>

#include <ctype.h>
#include <limits.h>
#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/rand.h>
#include <openssl/aes.h>

#include "log.h"
#include "xml_utils.h"
#include "parser.h"
#include "matcher.h"
#include "crypto.h"
#include "evernote.h"

#define MAX_NUM_MATCHES 32UL


static char* evernote_encrypt(const char* password, const char* plaintext) {

	int err = 0;
	byte_t salt[SALT_LEN] = {0};
	byte_t salt_hmac[SALT_HMAC_LEN] = {0};
	byte_t* key = NULL;
	byte_t* key_hmac = NULL;
	byte_t iv[IV_LEN] = {0};

	byte_t* ciphertext = NULL;
	size_t nbytes_ciphertext = 0;

	byte_t* data = NULL;
	size_t nbytes_data = 0;

	byte_t entire_hmac[ENTIRE_HMAC_LEN] = {0};

	byte_t* cursor = NULL;

	char* b64 = NULL;

	if (!password) {
		err = EINVAL;
		log_err("password should not be null or empty");
		goto error;
	}

	if (!plaintext) {
		err = EINVAL;
		log_err("plaintext should not be null or empty");
		goto error;
	}

	if (1 != RAND_bytes(salt, sizeof(salt))) {
		log_err("generate salt failed");
		goto error;
	}

	if (1 != RAND_bytes(salt_hmac, sizeof(salt_hmac))) {
		log_err("generate salt hmac failed");
		goto error;
	}

	key = pbkdf2_hmac_sha256(password, salt, SALT_LEN, PBKDF2_ITERATION, KEY_SIZE);
	if (!key) {
		log_err("derivate key from password and salt failed");
		goto error;
	}

	key_hmac = pbkdf2_hmac_sha256(password, salt_hmac, SALT_HMAC_LEN, PBKDF2_ITERATION, KEY_SIZE);
	if (!key) {
		log_err("derivate key hamc from password and salt hmac failed");
		goto error;
	}

	if (1 != RAND_bytes(iv, sizeof(iv))) {
		log_err("generate iv failed");
		goto error;
	}

	if (aes128_cbc_encrypt((const byte_t*)plaintext, strlen(plaintext), key, iv,
                           &ciphertext, &nbytes_ciphertext)) {
		log_err("encryption failed");
		goto error;
	}

	nbytes_data = EN_IDENTIFIER_LEN + SALT_LEN + SALT_HMAC_LEN + IV_LEN + nbytes_ciphertext + ENTIRE_HMAC_LEN;
	data = (byte_t*)malloc(nbytes_data * sizeof(byte_t));
	if (!data) {
		err = errno;
		log_err("alloc for data failed");
		goto error;
	}

	cursor = data;

	memcpy(cursor, "ENC0", EN_IDENTIFIER_LEN);
	cursor += EN_IDENTIFIER_LEN;

	memcpy(cursor, salt, SALT_LEN);
	cursor += SALT_LEN;

	memcpy(cursor, salt_hmac, SALT_HMAC_LEN);
	cursor += SALT_HMAC_LEN;

	memcpy(cursor, iv, IV_LEN);
	cursor += IV_LEN;

	memcpy(cursor, ciphertext, nbytes_ciphertext);
	cursor += nbytes_ciphertext;

	if (hmac_sha256(data, (size_t)(cursor-data), key_hmac, KEY_SIZE, entire_hmac)) {
		log_err("generate verify entire hmac failed");
		goto error;
	}
	memcpy(cursor, entire_hmac, ENTIRE_HMAC_LEN);
	cursor += ENTIRE_HMAC_LEN;

	b64 = base64_encode(data, nbytes_data);

	free(data);
	free(ciphertext);
	free(key_hmac);
	free(key);

	return b64;

error:

	if (b64) {
		free(b64);
	}

	if (data) {
		free(data);
	}

	if (ciphertext) {
		free(ciphertext);
	}

	if (key_hmac) {
		free(key_hmac);
	}

	if (key) {
		free(key);
	}

	errno = err;

	return NULL;
}


static xmlNodePtr encrypt_text(const char* password, const int is_web, const xmlChar* plaintext) {

	xmlNodePtr ret = NULL;

	char* ciphertext = evernote_encrypt(password, (const char*)plaintext);

	if (is_web) {
		ret = xmlNewNode(NULL, BAD_CAST"img");

		xmlNewProp(ret, BAD_CAST"src", BAD_CAST"/images/encrypted_text_button.gif");
		xmlNewProp(ret, BAD_CAST"name", BAD_CAST"en-crypt");
		xmlNewProp(ret, BAD_CAST"alt", (xmlChar*)ciphertext);
		xmlNewProp(ret, BAD_CAST"data-cipher", BAD_CAST"AES");
		xmlNewProp(ret, BAD_CAST"title", BAD_CAST"encrypted content");
		xmlNewProp(ret, BAD_CAST"data-hint", BAD_CAST"Pay one million or say goodby to your data!");
		xmlNewProp(ret, BAD_CAST"class", BAD_CAST"en-ignore");
		xmlNewProp(ret, BAD_CAST"style", BAD_CAST"cursor: pointer;");
	}
	else {
		ret = xmlNewNode(NULL, BAD_CAST"en-crypt");

		xmlNewProp(ret, BAD_CAST"cipher", BAD_CAST"AES");
		xmlNewProp(ret, BAD_CAST"length", BAD_CAST"128");
		xmlNewProp(ret, BAD_CAST"hint", BAD_CAST"Pay one million or say goodby to your data!");

		xmlAddChild(ret, xmlNewText((xmlChar*)ciphertext));
	}

	free(ciphertext);

	return ret;
}


/* collect text segments which were covered by the match  */
static int map_text_segments(const size_t match_start, const size_t match_end, const text_col_t col, text_segment_t** ret_segments, size_t* ret_num_segments) {

	int err = 0;

	size_t first_seg_index = 0;
	size_t last_seg_index = 0;
	text_segment_t* segments = NULL;
	size_t num_segments = 0;
	size_t i;

	for (i = 0; i < col.num_segments; ++i) {
		size_t seg_head = col.segments[i].offset;
		size_t seg_end = seg_head + col.segments[i].length - 1UL;

		if (seg_head <= match_start && match_start <= seg_end) {
			first_seg_index = i;
		}

		if (seg_head <= match_end && match_end <= seg_end) {
			last_seg_index = i;
		}
	}

	num_segments = last_seg_index-first_seg_index+1;

	if (!(segments = (text_segment_t*)malloc(num_segments * sizeof(text_segment_t)))) {
		err = errno;
		log_err("alloc for segments failed");
		goto error;
	}

	for (i = first_seg_index; i <= last_seg_index; ++i) {
		segments[i-first_seg_index] = col.segments[i];
	}

	*ret_segments = segments;
	*ret_num_segments = num_segments;

	return 0;

error:

	if (segments) {
		free(segments);
	}

	errno = err;

	return -1;
}


static int find_ancestors(const text_segment_t* segs, const size_t num_segs, xmlNodePtr* ret_lca, xmlNodePtr* ret_fa, xmlNodePtr* ret_la) {

	int err = 0;

	xmlNodePtr lca = NULL;
	xmlNodePtr first_ancestor = NULL;
	xmlNodePtr last_ancestor = NULL;
	xmlNodePtr n = NULL;
	size_t i;

	if (!segs || num_segs < 2) {
		err = EINVAL;
		log_err("segments must be more than 1(exclusive)");
		goto error;
	}

	lca = segs[0].node;

	for (i = 1; i < num_segs; ++i) {
		lca = find_lca(lca, segs[i].node);
	}
	log_info("common ancestor: %s", lca->name);

	n = segs[0].node;
	while (n->parent && n->parent != lca) {
		n = n->parent;
	}
	first_ancestor = n;
	log_info("first node ancestor: %s", first_ancestor->name);

	n = segs[num_segs-1].node;
	while (n->parent && n->parent != lca) {
		n = n->parent;
	}
	last_ancestor = n;
	log_info("last node ancestor: %s", last_ancestor->name);

	*ret_lca = lca;
	*ret_fa = first_ancestor;
	*ret_la = last_ancestor;

	return 0;

error:

	errno = err;

	return -1;
}

/* collect branches under lca and between first_ancestor and last_ancestor  */
static xmlNodePtr build_en_crypt_node(const char* password, const int is_web, const xmlNodePtr lca, const xmlNodePtr first_ancestor, const xmlNodePtr last_ancestor) {

	int err = 0;

	xmlNodePtr n = NULL;
	xmlNodePtr cloned_lca = NULL;
	xmlNodePtr top_span = NULL;

	xmlBufferPtr buffer = NULL;
	xmlNodePtr en_crypt_node = NULL;

	if (!password) {
		err = EINVAL;
		log_err("password should not be null");
		goto error;
	}

	if (!lca) {
		err = EINVAL;
		log_err("lca should not be null");
		goto error;
	}

	if (!first_ancestor) {
		err = EINVAL;
		log_err("first ancestor not be null");
		goto error;
	}

	if (!last_ancestor) {
		err = EINVAL;
		log_err("last ancestor should not be null");
		goto error;
	}

	/* clone lca recursively, and then remove all children */
	/* if we clone lca non-recursively, we will lost the properties/namespaces of lca itself */
	if (!(cloned_lca = xmlCopyNode(lca, 1))) {
		log_err("copy lca failed");
		goto error;
	}

	n = cloned_lca->children;

	while(n) {
		xmlNodePtr next = n->next;
		xmlUnlinkNode(n);
		xmlFreeNode(n);
		n = next;
	}

	/* walk from first_ancestor to last_ancestor, and copy each of them into the cloned lca */
	n = first_ancestor;

	do {
		xmlAddChild(cloned_lca, xmlCopyNode(n, 1));
	} while(n != last_ancestor && (n = n->next));

	if (!(top_span = xmlNewNode(NULL, BAD_CAST"span"))) {
		log_err("create top span node failed");
		goto error;
	}

	/* wrap the cloned lca with <span>, according to evernote client's behavior  */
	xmlAddChild(top_span, cloned_lca);
	xmlNewProp(top_span, BAD_CAST"style", BAD_CAST"border-collapse: separate; color: rgb(0, 0, 0); font-family: Arial; font-style: normal; font-variant: normal; font-weight: normal; letter-spacing: normal; line-height: normal; orphans: 2; text-align: -webkit-auto; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-border-horizontal-spacing: 0px; -webkit-border-vertical-spacing: 0px; -webkit-text-decorations-in-effect: none; -webkit-text-size-adjust: auto; -webkit-text-stroke-width: 0px; font-size: medium; ");

	/* encrypt the span node and put the ciphertext under en-crypt node  */
	buffer = xmlBufferCreate();
	xmlNodeDump(buffer, top_span->doc, top_span, 0, 0);

	en_crypt_node = encrypt_text(password, is_web, buffer->content);

	xmlBufferFree(buffer);

	xmlUnlinkNode(cloned_lca);
	xmlFreeNode(cloned_lca);

	xmlUnlinkNode(top_span);
	xmlFreeNode(top_span);

	return en_crypt_node;

error:

	if (en_crypt_node) {
		xmlUnlinkNode(en_crypt_node);
		xmlFreeNode(en_crypt_node);
	}

	if (buffer) {
		xmlBufferFree(buffer);
	}

	if (top_span) {
		xmlUnlinkNode(top_span);
		xmlFreeNode(top_span);
	}

	if (cloned_lca) {
		xmlUnlinkNode(cloned_lca);
		xmlFreeNode(cloned_lca);
	}

	errno = err;

	return NULL;
}

/* trim branches under lca, but not between first_ancestor and last_ancestor  */
static int trim_branches(const xmlNodePtr lca, xmlNodePtr first_ancestor, xmlNodePtr last_ancestor) {

	int err = 0;
	xmlNodePtr n = NULL;

	if (!lca) {
		err = EINVAL;
		log_err("lca should not be null");
		goto error;
	}

	if (!first_ancestor) {
		err = EINVAL;
		log_err("first ancestor not be null");
		goto error;
	}

	if (!last_ancestor) {
		err = EINVAL;
		log_err("last ancestor should not be null");
		goto error;
	}

	n = first_ancestor;

	while(1) {
		xmlNodePtr next = n->next;

		if (n == last_ancestor) {
			xmlUnlinkNode(n);
			xmlFreeNode(n);
			break;
		}
		else {
			xmlUnlinkNode(n);
			xmlFreeNode(n);
		}

		n = next;
	}

	return 0;

error:

	errno = err;

	return -1;
}


static int build_branch(const char* password, const int is_web, const text_segment_t* segs, const size_t num_segs, const size_t prefix_frag, const size_t postfix_frag) {

	int err = 0;

	xmlNodePtr lca = NULL;
	xmlNodePtr first_ancestor = NULL;
	xmlNodePtr last_ancestor = NULL;
	xmlNodePtr en_crypt_node = NULL;

	if (!password) {
		err = EINVAL;
		log_err("password should not be null");
		goto error;
	}

	if (!segs || num_segs < 2) {
		err = EINVAL;
		log_err("segments not be null");
		goto error;
	}

	if (find_ancestors(segs, num_segs, &lca, &first_ancestor, &last_ancestor)) {
		log_err("finding ancestors failed");
		goto error;
	}

	/* the match text doesn't fully aligned with first text node */
	if (prefix_frag > 0) {

		const xmlChar* text = segs[0].node->content;

		xmlChar* non_sens_frag = xmlStrsub(text, 0, (int)prefix_frag);
		xmlChar* sens_frag = xmlStrsub(text, (int)prefix_frag, xmlStrlen(text)-(int)prefix_frag);

		xmlNodePtr cloned_first_ancestor = NULL;
		xmlNodePtr non_sens_frag_node = NULL;
		xmlNodePtr sens_frag_node = NULL;

		/* clone the tree, and remove the last half(sensitive part) of the fragmented node */
		if (!(cloned_first_ancestor = xmlCopyNode(first_ancestor, 1))) {
			log_err("copy first ancestor failed");
			goto error;
		}

		if (find_boundry_text_nodes(cloned_first_ancestor, NULL, &non_sens_frag_node)) {
			log_err("finding boundry text nodes failed");
			goto error;
		}
		xmlNodeSetContent(non_sens_frag_node, non_sens_frag);

		xmlAddPrevSibling(first_ancestor, cloned_first_ancestor);


		/* remove the first half(non sensitive part) of the sensitive node */
		if (find_boundry_text_nodes(first_ancestor, &sens_frag_node, NULL)) {
			log_err("finding boundry text nodes failed");
			goto error;
		}
		xmlNodeSetContent(sens_frag_node, sens_frag);

		xmlFree(non_sens_frag);
		xmlFree(sens_frag);
	}

	if (postfix_frag > 0) {

		const xmlChar* text = segs[num_segs-1].node->content;

		xmlChar* sens_frag = xmlStrsub(text, 0, xmlStrlen(text)-(int)postfix_frag);
		xmlChar* non_sens_frag = xmlStrsub(text, xmlStrlen(sens_frag), (int)postfix_frag);

		xmlNodePtr cloned_last_ancestor = NULL;
		xmlNodePtr non_sens_frag_node = NULL;
		xmlNodePtr sens_frag_node = NULL;

		/* clone the tree, and remove the first half(sensitive part) of the fragmented node */
		if (!(cloned_last_ancestor = xmlCopyNode(last_ancestor, 1))) {
			log_err("copy last ancestor failed");
			goto error;
		}

		if (find_boundry_text_nodes(cloned_last_ancestor, &non_sens_frag_node, NULL)) {
			log_err("finding boundry text nodes failed");
			goto error;
		}

		xmlNodeSetContent(non_sens_frag_node, non_sens_frag);

		xmlAddNextSibling(last_ancestor, cloned_last_ancestor);


		/* remove the last half(non sensitive part) of the sensitive node */
		if (find_boundry_text_nodes(last_ancestor, NULL, &sens_frag_node)) {
			log_err("finding boundry text nodes failed");
			goto error;
		}

		xmlNodeSetContent(sens_frag_node, sens_frag);

		xmlFree(non_sens_frag);
		xmlFree(sens_frag);
	}

	if (!(en_crypt_node = build_en_crypt_node(password, is_web, lca, first_ancestor, last_ancestor))) {
		log_err("build en-crypt node failed");
		goto error;
	}

	xmlAddPrevSibling(first_ancestor, en_crypt_node);

	if (trim_branches(lca, first_ancestor, last_ancestor)) {
		log_err("prune sensitive nodes failed");
		goto error;
	}

	return 0;

error:

	if (en_crypt_node) {
		xmlUnlinkNode(en_crypt_node);
		xmlFreeNode(en_crypt_node);
	}

	errno = err;

	return -1;
}


static int rebuild_dom(const text_col_t col, const match_t* matches, const size_t num_matches, const char* password, const int is_web) {

	size_t i;

	for (i = 0; i < num_matches; ++i) {
		const size_t match_start = matches[i].start;
		const size_t match_end = matches[i].end;

		text_segment_t* segs = NULL;
		size_t num_segs = 0;
		text_segment_t first_seg, last_seg;

		/* the number of characters while the first text segment is not fully aligned */
		size_t prefix_frag = 0;
		/* the number of characters while the last text segment is not fully aligned */
		size_t postfix_frag = 0;

		if (map_text_segments(match_start, match_end, col, &segs, &num_segs)) {
			log_err("collect segments of one match failed");
			goto error;
		}

		first_seg = segs[0];
		last_seg = segs[num_segs-1];

		prefix_frag = match_start - first_seg.offset;
		postfix_frag = last_seg.offset + last_seg.length - match_end - 1;

		if (num_segs == 1) {	/* the match text is within a single node */

			const text_segment_t seg = first_seg;
			const size_t seg_start = seg.offset;

			/* the match part occupies the middle of node text */
			if (prefix_frag > 0 && postfix_frag > 0) {
				xmlChar* prefix = xmlStrndup(col.text + seg_start, (int)prefix_frag);
				xmlChar* postfix = xmlStrndup(col.text + match_end + 1, (int)postfix_frag);
				xmlNodePtr postfix_node = xmlNewText(postfix);

				xmlChar* plaintext = xmlStrndup(col.text + match_start, (int)(match_end - match_start + 1));
				xmlNodePtr en_crypt_node = encrypt_text(password, is_web, plaintext);

				xmlNodeSetContent(seg.node, prefix);
				xmlAddNextSibling(seg.node, en_crypt_node);
				xmlAddNextSibling(en_crypt_node, postfix_node);

				xmlFree(plaintext);
				xmlFree(postfix);
				xmlFree(prefix);
			}
			/* the match part occupies only the head of node text */
			else if (prefix_frag > 0 && postfix_frag <= 0) {
				xmlChar* prefix = xmlStrndup(col.text + seg_start, (int)prefix_frag);

				xmlChar* plaintext = xmlStrndup(col.text + match_start, (int)(match_end - match_start + 1));
				xmlNodePtr en_crypt_node = encrypt_text(password, is_web, plaintext);

				xmlNodeSetContent(seg.node, prefix);
				xmlAddNextSibling(seg.node, en_crypt_node);

				xmlFree(plaintext);
				xmlFree(prefix);
			}
			/* the match part occupies only the tail of node text */
			else if (prefix_frag == 0 && postfix_frag > 0) {
				xmlChar* postfix = xmlStrndup(col.text + match_end + 1, (int)postfix_frag);

				xmlChar* plaintext = xmlStrndup(col.text + match_start, (int)(match_end - match_start + 1));
				xmlNodePtr en_crypt_node = encrypt_text(password, is_web, plaintext);

				xmlNodeSetContent(seg.node, postfix);
				xmlAddPrevSibling(seg.node, en_crypt_node);

				xmlFree(plaintext);
				xmlFree(postfix);
			}
			else {	/* the match part covers whole node text */
				xmlNodePtr parent_node = seg.node->parent;
				xmlBufferPtr buffer = xmlBufferCreate();
				xmlNodePtr en_crypt_node = NULL;

				xmlNodeDump(buffer, parent_node->doc, parent_node, 0, 0);
				en_crypt_node = encrypt_text(password, is_web, buffer->content);

				xmlAddPrevSibling(parent_node, en_crypt_node);

				xmlBufferFree(buffer);

				xmlUnlinkNode(parent_node);
				xmlFreeNode(parent_node);
			}
		}
		else {	/* the matched text is cross multiple nodes */
			if (build_branch(password, is_web, segs, num_segs, prefix_frag, postfix_frag)) {
				log_err("build branch failed");
				goto error;
			}
		}

		free(segs);
	}

	return 0;

error:

	return -1;
}


char* evernote_lock_note(const char* password, const char* note, const int is_web) {

	int err = 0;

	xmlDocPtr doc = NULL;
	text_col_t col;
	match_t matches[MAX_NUM_MATCHES];
	int found = 0;
	size_t i;
	xmlChar* locked_note = NULL;
	int nbytes_locked_note = 0;

	memset(&col, 0, sizeof(col));


	if (!password) {
		err = EINVAL;
		log_err("password should not be null");
		goto error;
	}

	if (!note) {
		err = EINVAL;
		log_err("note should not be null");
		goto error;
	}

	doc = parse_html(note, strlen(note));

	if (!doc) {
		log_err("parse html failed");
		goto error;
	}

	col = collect_text(doc);

	for (i = 0UL; i < col.num_segments; ++i) {
		text_segment_t seg = col.segments[i];
		xmlChar* tmp = xmlStrndup(col.text + seg.offset, (int)seg.length + 1);
		tmp[seg.length] = '\0';
		xmlFree(tmp);
	}

	found = match_credit_card((const char*)col.text, matches, MAX_NUM_MATCHES);

	if (found > 0) {
		rebuild_dom(col, matches, (size_t)found, password, is_web);
	}

	xmlDocDumpMemory(doc, &locked_note, &nbytes_locked_note);

	if (!(locked_note = (xmlChar*)realloc(locked_note, ((size_t)nbytes_locked_note+1) * sizeof(xmlChar)))) {
		err = errno;
		log_err("realloc for locked_note failed");
		goto error;
	}

	locked_note[nbytes_locked_note] = (xmlChar)'\0';

	release_text_col(col);
	xmlFreeDoc(doc);

	return (char*)locked_note;

error:

	if (locked_note) {
		xmlFree(locked_note);
	}

	release_text_col(col);

	if (doc) {
		xmlFreeDoc(doc);
	}

	errno = err;

	return NULL;
}

