#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <stdarg.h>
#include <pthread.h>

#include "log.h"


struct _log_context_t {
	log_level_t level;
	char file[256];
	char function[256];
	unsigned int line;
};

typedef struct _log_context_t log_context_t;



static void native_log(const log_level_t level, const char* file, const char* function, const unsigned int line, const char* text) {
	static const char* log_level_str[] = {"", "info", "warn", "error"};
	fprintf(stderr, "(%s) %s (%s:%s:%u)\n", log_level_str[level], text, file, function, line);
}



#if (__STDC_VERSION__ >= 199901L)

void _log_c99_impl(const log_level_t level, const char* file, const char* function, const unsigned line, const char* format, ...) {

#if defined(_DEBUG)
	char buffer[256];

	va_list ap;
	va_start(ap, format);
	vsnprintf(buffer, sizeof(buffer), format, ap);
	va_end(ap);

	native_log(level, file, function, line, buffer);
#endif
}

#else

static __thread log_context_t tls_log_context;

static void log_impl(const char* format, ...) {
#if defined(_DEBUG)
	char buffer[256];

	va_list ap;
	va_start(ap, format);
	vsnprintf(buffer, sizeof(buffer), format, ap);
	va_end(ap);

	native_log(tls_log_context.level, tls_log_context.file, tls_log_context.function, tls_log_context.line, buffer);
#endif
}

log_func_t _log_wrapper(const log_level_t level, const char* file, const char* function, const unsigned int line) {
#if defined(_DEBUG)
	tls_log_context.level = level;
	strcpy(tls_log_context.file, file);
	strcpy(tls_log_context.function, function);
	tls_log_context.line = line;
#endif
	return &log_impl;
}

#endif

