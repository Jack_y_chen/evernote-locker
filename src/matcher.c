#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <errno.h>

#include <regex.h>

#include "log.h"
#include "matcher.h"

#define CREDITCARD_PATTERN "\\<[0-9]{4}\\-[0-9]{4}\\-[0-9]{4}\\-[0-9]{4}\\>"
#define CREDITCARD_PATTERN "[0-9]{4}\\-[0-9]{4}\\-[0-9]{4}\\-[0-9]{4}"

int match_credit_card(const char* text, match_t matches[], const size_t nmatches) {

	int err = 0;
	regex_t reg;
	const char* p = NULL;
	size_t found = 0UL;

	if (!text) {
		err = EINVAL;
		log_err("text should not be null");
		goto error;
	}

	memset(&reg, 0, sizeof(reg));

	if (regcomp(&reg, CREDITCARD_PATTERN, REG_EXTENDED | REG_ICASE)) {
		err = EINVAL;
		log_err("compile regex failed");
		goto error;
	}

	p = text;

	while(1) {
		regmatch_t m[1];
		const int result = regexec(&reg, p, 1UL, m, 0);

		if (result == REG_NOMATCH) {
			break;
		}
		else if (result == 0) {

			const size_t start = (size_t)(m[0].rm_so);
			const size_t end = (size_t)(m[0].rm_eo - 1);

			if (found+1 < nmatches) {
				match_t pos;
				pos.start = (size_t)start + (size_t)(p - text);
				pos.end = (size_t)end + (size_t)(p - text);
				matches[found++] = pos;
			}
			else {
				break;
			}

			p = p + end;
		}
		else if (result == REG_ESPACE) {
			err = ENOMEM;
			log_err("regex ran out of memory");
			goto error;
		}
	}

	regfree(&reg);

	return (int)found;

error:

	regfree(&reg);

	errno = err;

	return -1;
}

