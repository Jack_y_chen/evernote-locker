#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <errno.h>

#include "libxml/HTMLparser.h"

#include "log.h"
#include "xml_utils.h"


#define XML_ENCODING "utf-8"

#define XML_PARSER_FLAGS (XML_PARSE_RECOVER | XML_PARSE_NOERROR | \
						  XML_PARSE_NOWARNING | XML_PARSE_NOBLANKS | \
                          XML_PARSE_NONET | XML_PARSE_COMPACT)

#define HTML_PARSER_FLAGS (HTML_PARSE_RECOVER | HTML_PARSE_NOERROR | \
                           HTML_PARSE_NOWARNING | HTML_PARSE_NOBLANKS | \
                           HTML_PARSE_NONET)


xmlDocPtr parse_xml(const void* data, const size_t nbytes) {
	xmlDocPtr doc = xmlReadMemory((const char*)data, (int)nbytes, NULL, XML_ENCODING, XML_PARSER_FLAGS);

	if (!doc) {
		const xmlErrorPtr last_err = xmlGetLastError();
		log_err("xml parsing failed: %s", last_err ? last_err->message : "unknown error");
	}

	return doc;
}

xmlDocPtr parse_html(const void* data, const size_t nbytes) {
	xmlDocPtr doc = htmlReadMemory((const char*)data, (int)nbytes, NULL, XML_ENCODING, HTML_PARSER_FLAGS);

	if (!doc) {
		const xmlErrorPtr last_err = xmlGetLastError();
		log_err("html parsing failed: %s", last_err ? last_err->message : "unknown error");
	}

	return doc;
}


int traverse_nodes(const xmlNodePtr node, const node_traverse_func_t func, void* userdata) {

	int err = 0;

	if (!node) {
		err = EINVAL;
		log_err("node should not be null");
		goto error;
	}

	if (!func) {
		err = EINVAL;
		log_err("iter func should not be null");
		goto error;
	}

	func(node, userdata);

	if (node->type == XML_ELEMENT_NODE) {

		xmlNodePtr child = NULL;

		child = node->xmlChildrenNode;

		while(child) {

			if (traverse_nodes(child, func, userdata)) {
				err = errno;
				log_err("traverse node failed");
				goto error;
			}

			child = child->next;
		}
	}

	return 0;

error:

	errno = err;

	return -1;
}


static int get_node_depth(xmlNodePtr n) {

	int depth = 0;

	while (n && n != xmlDocGetRootElement(n->doc)) {
		++depth;
		n = n->parent;
	}

	return depth;
}

xmlNodePtr find_lca(const xmlNodePtr p, const xmlNodePtr q) {

	int err = 0;
	int p_depth = 0;
	int q_depth = 0;
	xmlNodePtr pa, qa;

	if (!p) {
		err = EINVAL;
		log_err("p should not be null");
		goto error;
	}

	if (!q) {
		err = EINVAL;
		log_err("q should not be null");
		goto error;
	}


	pa = p;
	p_depth = get_node_depth(p);

	qa = q;
	q_depth = get_node_depth(q);

	while (p_depth > q_depth) {
		pa = pa->parent;
		--p_depth;
	}

	while (q_depth > p_depth) {
		qa = qa->parent;
		--q_depth;
	}

	/* now, pa and qa are at the same depth */
	while (pa != qa) {
		pa = pa->parent;
		qa = qa->parent;
	}

	return pa;

error:

	errno = err;

	return NULL;
}




typedef struct {
	xmlNodePtr first;
	xmlNodePtr last;
} fbt_userdata;

static void fbt_callback(const xmlNodePtr node, void* userdata) {

	if (node->type == XML_TEXT_NODE) {

		fbt_userdata* u = (fbt_userdata*)userdata;

		if (!u->first) {
			u->first = node;
		}

		u->last = node;
	}
}

int find_boundry_text_nodes(const xmlNodePtr root, xmlNodePtr* ret_first_textnode, xmlNodePtr* ret_last_textnode) {

	fbt_userdata u = {NULL, NULL};

	traverse_nodes(root, fbt_callback, &u);

	if (ret_first_textnode) {
		*ret_first_textnode = u.first;
	}

	if (ret_last_textnode) {
		*ret_last_textnode = u.last;
	}

	return 0;
}

