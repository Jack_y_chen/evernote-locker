#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <errno.h>
#include <ctype.h>

#include "log.h"
#include "xml_utils.h"
#include "parser.h"


typedef struct {
	size_t text_length;
	size_t num_segments;
} text_counter_userdata;


static xmlChar* trim(const xmlChar* in) {

	int err = 0;

	xmlChar* out = NULL;

	int i, j;


	if (!in) {
		err = EINVAL;
		log_err("input string cannot be null");
		goto error;
	}

	for (i = 0; i < xmlStrlen(in); ++i) {
		const xmlChar c = in[i];

		if (!(isspace(c))) {
			break;
		}
	}

	for (j = xmlStrlen(in) - 1; j > i; --j) {
		const xmlChar c = in[j];

		if (!(isspace((char)c))) {
			break;
		}
	}

	return xmlStrsub(in, i, j - i + 1);

error:

	if (out) {
		xmlFree(out);
		out = NULL;
	}

	errno = err;

	return NULL;
}

static void text_counter(const xmlNodePtr node, void* userdata) {

	if (!node) {
		return;
	}
	else if (node->type == XML_ELEMENT_NODE && !xmlStrcasecmp(node->name, BAD_CAST"br")) {
		text_counter_userdata* u = (text_counter_userdata*)userdata;
		u->text_length += 1UL;
	}
	else if (node->type == XML_ELEMENT_NODE && !xmlStrcasecmp(node->name, BAD_CAST"p")) {
		text_counter_userdata* u = (text_counter_userdata*)userdata;
		u->text_length += 2UL;
	}
/*
	else if (node->type == XML_ELEMENT_NODE && !xmlStrcasecmp(node->name, BAD_CAST"div")) {
		text_counter_userdata* u = (text_counter_userdata*)userdata;
		u->text_length += 1UL;
	}
*/
	else if (node->type == XML_TEXT_NODE) {
		text_counter_userdata* u = (text_counter_userdata*)userdata;

		xmlChar* trimmed = trim(node->content);

		if (trimmed) {
			u->text_length += (size_t)xmlStrlen(trimmed);
			++u->num_segments;
			xmlFree(trimmed);
		}
	}
}

static void text_collector(const xmlNodePtr node, void* userdata) {

	if (!node) {
		return;
	}
	else if (node->type == XML_ELEMENT_NODE && !xmlStrcasecmp(node->name, BAD_CAST"br")) {
		text_col_t* c = (text_col_t*)userdata;
		*(c->_p++) = '\n';
	}
	else if (node->type == XML_ELEMENT_NODE && !xmlStrcasecmp(node->name, BAD_CAST"p")) {
		text_col_t* c = (text_col_t*)userdata;
		*(c->_p++) = '\n';
		*(c->_p++) = '\n';
	}
/*
	else if (node->type == XML_ELEMENT_NODE && !xmlStrcasecmp(node->name, BAD_CAST"div")) {
		text_col_t* c = (text_col_t*)userdata;
		*(c->_p++) = '\n';
	}
*/
	else if (node->type == XML_TEXT_NODE) {
		text_col_t* c = (text_col_t*)userdata;

		xmlChar* trimmed = trim(node->content);

		if (trimmed) {
			const size_t len = (size_t)xmlStrlen(trimmed);

			text_segment_t seg;
			seg.offset = (size_t)(c->_p - c->text);
			seg.length = len;
			seg.node = node;

			/* xmlStrcat(u->text, node->content); */
			strcpy((char*)c->_p, (const char*)trimmed);
			c->_p += (int)len;

			c->segments[c->_i++] = seg;

			xmlFree(trimmed);
		}
	}
}

text_col_t collect_text(const xmlDocPtr doc) {

	text_counter_userdata u;
	text_col_t ret;

	memset(&u, 0, sizeof(u));
	memset(&ret, 0, sizeof(ret));

	if (traverse_nodes(xmlDocGetRootElement(doc), text_counter, &u)) {
		log_err("traverse nodes failed");
		goto error;
	}

	ret.text = (xmlChar*)malloc(((size_t)u.text_length + 1) * sizeof(xmlChar));
	ret._p = ret.text;
	ret.num_segments = u.num_segments;
	ret.segments = (text_segment_t*)malloc(u.num_segments * sizeof(text_segment_t));

	if (traverse_nodes(xmlDocGetRootElement(doc), text_collector, &ret)) {
		log_err("traverse nodes failed");
		goto error;
	}

	return ret;

error:

	return ret;
}


void release_text_col(text_col_t col) {
	xmlFree(col.text);
	free(col.segments);
}
