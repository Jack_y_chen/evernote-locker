#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <errno.h>

#include <regex.h>

#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/rand.h>
#include <openssl/aes.h>

#include "log.h"
#include "crypto.h"
#include "evernote.h"
#include "parser.h"


#define ENCRYPT_NODE_PATTERN "<en-crypt[^>]*>([a-zA-Z0-9\\+/=]+)</en-crypt>"
#define IMG_PATTERN "<img.*? alt=\"([a-zA-Z0-9\\+/=]+)\".*? name=\"en-crypt\" data-cipher=\"AES\".*?>"

static void dump_bytes(const char* tag, const byte_t* bytes, const size_t len) {

#if defined(_DEBUG)
	size_t i;

	for (i = 0; i < len; ++i) {
		if (i == 0) {
			fprintf(stderr, "%s(%lu): [0x%02x,", tag, len, bytes[i]);
		}
		else if (i == len - 1) {
			fprintf(stderr, "0x%02x]\n", bytes[i]);
		}
		else {
			fprintf(stderr, "0x%02x,", bytes[i]);
		}
	}
#endif
}

static char* evernote_decrypt(const char* password, const char* b64_data) {

	int err = 0;

	byte_t* data = NULL;
	size_t nbytes_data = 0UL;

	const byte_t* cursor = NULL;

	const byte_t* en_id = NULL;
	const byte_t* salt = NULL;
	const byte_t* salt_hmac = NULL;
	const byte_t* iv = NULL;

	const byte_t* ciphertext = NULL;
	size_t nbytes_ciphertext = 0;

	const byte_t* entire_hmac = NULL;

	byte_t* key_hmac = NULL;

	byte_t verify_entire_hmac[ENTIRE_HMAC_LEN] = {0};

	byte_t* key = NULL;

	byte_t* plaintext = NULL;
	size_t nbytes_plaintext = 0;


	if (!password) {
		err = EINVAL;
		log_err("password should not be null or empty");
		goto error;
	}

	if (!b64_data) {
		err = EINVAL;
		log_err("data should not be null or empty");
		goto error;
	}

	if (base64_decode(b64_data, &data, &nbytes_data)) {
		log_err("base64 decode data failed");
		goto error;
	}

	cursor = data;

	/* check evernote identifier */
	en_id = cursor;

	if (memcmp(en_id, "ENC0", EN_IDENTIFIER_LEN)) {
		err = EINVAL;
		log_err("data is not evernote encrypted data");
		goto error;
	}

	cursor += EN_IDENTIFIER_LEN;

	/* salt */
	salt = cursor;
	dump_bytes("salt", salt, SALT_LEN);
	cursor += SALT_LEN;

	/* salt_hmac */
	salt_hmac = cursor;
	dump_bytes("salt_hmac", salt_hmac, SALT_HMAC_LEN);
	cursor += SALT_HMAC_LEN;

	/* iv */
	iv = cursor;
	dump_bytes("iv", iv, IV_LEN);
	cursor += IV_LEN;

	/* ciphertext */
	nbytes_ciphertext = nbytes_data - (size_t)(cursor-data) - ENTIRE_HMAC_LEN;
	ciphertext = cursor;
/*
	dump_bytes("ciphertext", ciphertext, nbytes_ciphertext);
*/
	cursor += nbytes_ciphertext;

	/* entire_hmac */
	entire_hmac = cursor;
	dump_bytes("entire_hmac", entire_hmac, ENTIRE_HMAC_LEN);
	cursor += ENTIRE_HMAC_LEN;

	/* do hmac verification of whole data */
	key_hmac = pbkdf2_hmac_sha256(password, salt_hmac, SALT_HMAC_LEN, PBKDF2_ITERATION, KEY_SIZE);

	if (!key_hmac) {
		log_err("derivate key hamc from password and salt hmac failed");
		goto error;
	}

	dump_bytes("key_hmac", key_hmac, KEY_SIZE);

	if (hmac_sha256(data, nbytes_data-ENTIRE_HMAC_LEN, key_hmac, KEY_SIZE, verify_entire_hmac)) {
		log_err("generate verify entire hmac failed");
		goto error;
	}

	dump_bytes("verify_entire_hmac", verify_entire_hmac, sizeof(verify_entire_hmac));

	if (memcmp(verify_entire_hmac, entire_hmac, ENTIRE_HMAC_LEN)) {
		err = EINVAL;
		log_err("entire hmac verification failed");
		goto error;
	}

	key = pbkdf2_hmac_sha256(password, salt, SALT_LEN, PBKDF2_ITERATION, KEY_SIZE);
	if (!key) {
		log_err("derivate key from password and salt failed");
		goto error;
	}

	dump_bytes("key", key, KEY_SIZE);

	if (aes128_cbc_decrypt(ciphertext, nbytes_ciphertext, key, iv, &plaintext, &nbytes_plaintext)) {
		log_err("decryption failed");
		goto error;
	}

	plaintext[nbytes_plaintext] = '\0';

	free(key);
	free(key_hmac);
	free(data);

	return (char*)plaintext;

error:

	if (plaintext) {
		free(plaintext);
	}

	if (key) {
		free(key);
	}

	if (key_hmac) {
		free(key_hmac);
	}

	if (data) {
		free(data);
	}

	errno = err;

	return NULL;
}



typedef struct {
	const char* pos;
	size_t len;
	char* plaintext;
} en_tag_match_t;


char* evernote_unlock_note(const char* password, const char* note, const int is_web) {

	int err = 0;

	const char* pattern = is_web ? IMG_PATTERN : ENCRYPT_NODE_PATTERN;
	regex_t reg;
	const char* p = NULL;

	en_tag_match_t* matches = NULL;
	size_t max_matches = 0UL;
	size_t found = 0;

	size_t i;

	char* unlocked_note = NULL;
	size_t nbytes_unlocked_note = 0;

	const char* r = NULL;
	char* w = NULL;

	memset(&reg, 0, sizeof(reg));


	if (!password) {
		err = EINVAL;
		log_err("password should not be null");
		goto error;
	}

	if (!note) {
		err = EINVAL;
		log_err("note should not be null");
		goto error;
	}

	if (regcomp(&reg, pattern, REG_EXTENDED | REG_ICASE)) {
		log_err("compile regex failed");
		goto error;
	}

	if (!(matches = (en_tag_match_t*)malloc(8 * sizeof(en_tag_match_t)))) {
		err = errno;
		log_err("alloc for matches failed");
		goto error;
	}
	max_matches = 8;

	p = note;

	while(1) {

		regmatch_t local_matches[2];
		const int result = regexec(&reg, p, 2UL, local_matches, 0);

		if (result == REG_NOMATCH) {
			break;
		}
		else if (result == 0) {
            const size_t whole_match_start = (size_t)local_matches[0].rm_so;
            const size_t whole_match_end = (size_t)local_matches[0].rm_eo;
            const size_t whole_match_len = whole_match_end - whole_match_start;

            const size_t sub_match_start = (size_t)local_matches[1].rm_so;
			const size_t sub_match_end = (size_t)local_matches[1].rm_eo;
            const size_t sub_match_len = sub_match_end - sub_match_start;

			en_tag_match_t m;

			char* data = NULL;

			if (found == max_matches) {

				if (!(matches = (en_tag_match_t*)realloc(matches, 2 * max_matches * sizeof(en_tag_match_t)))) {
					err = errno;
					log_err("re-alloc for matches failed");
					goto error;
				}

				max_matches *= 2;
			}

			if (!(data = (char*)malloc((sub_match_len+1) * sizeof(char)))) {
				err = errno;
				log_err("alloc for plaintext buffer failed");
				goto error;
			}

			memcpy(data, p + sub_match_start, sub_match_len);
			data[sub_match_len] = '\0';

			m.pos = p + whole_match_start;
			m.len = whole_match_len;
			m.plaintext = evernote_decrypt(password, data);

			if (m.plaintext) {
				matches[found++] = m;
			}

			free(data);

			p = p + whole_match_end;
		}
		else if (result == REG_ESPACE) {
            err = ENOMEM;
            log_err("regex ran out of memory");
            goto error;
		}
	}

	nbytes_unlocked_note = strlen(note) + 1;

	for (i = 0; i < found; ++i) {
		const en_tag_match_t m = matches[i];
		nbytes_unlocked_note -= m.len;
		nbytes_unlocked_note += strlen(m.plaintext);
	}

	if (!(unlocked_note = (char*)malloc(nbytes_unlocked_note * sizeof(char)))) {
		err = errno;
		log_err("alloc for unlocked_note failed");
		goto error;
	}

	r = note;
	w = unlocked_note;

	for (i = 0; i < found; ++i) {
		const en_tag_match_t m = matches[i];

		memcpy(w, r, (size_t)(m.pos - r));
		w += m.pos - r;
		r = m.pos + m.len;

		memcpy(w, m.plaintext, strlen(m.plaintext));
		w += strlen(m.plaintext);
	}

	memcpy(w, r, (size_t)(note + strlen(note) - r));
	w += note + strlen(note) - r;

	*w = '\0';

	for (i = 0; i < found; ++i) {
		free(matches[i].plaintext);
	}
	free(matches);

	regfree(&reg);

	return (char*)unlocked_note;

error:

	if (unlocked_note) {
		free(unlocked_note);
	}

	if (matches) {
		for (i = 0; i < found; ++i) {
			free(matches[i].plaintext);
		}

		free(matches);
	}

	regfree(&reg);

	errno = err;

	return NULL;
}

