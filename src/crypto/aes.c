#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <errno.h>

#include <openssl/evp.h>
#include <openssl/aes.h>

#include "log.h"
#include "crypto.h"

/*
int decryptccm_aes256(const byte_t* ciphertext, const size_t nbytes_ciphertext,
                      const byte_t* aad, const size_t aad_len,
	                  const byte_t* tag,
	                  const byte_t key[], const byte_t* iv,
	                  byte_t** ret_plaintext, size_t* ret_nbytes_plaintext) {
{
	int err = 0;
	EVP_CIPHER_CTX *ctx = NULL;
	int len = 0;
	int lol = 2;

	if (!(ctx = EVP_CIPHER_CTX_new())) {
		log_err("init evp context failed");
		goto error;
	}

	if(1 != EVP_DecryptInit_ex(ctx, EVP_aes_256_ccm(), NULL, NULL, NULL)) {
		log_err("init evp aes256 context failed");
		goto error;
	}

	if (nbytes_ciphertext >= 1<<16) lol++;
	if (nbytes_ciphertext >= 1<<24) lol++;

	if(1 != EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_CCM_SET_IVLEN, 15-lol, NULL)) {
		log_err("set iv length failed");
		goto error;
	}

	if(1 != EVP_CIPHER_CTX_ctrl(ctx, EVP_CTRL_CCM_SET_TAG, 8, (void*)tag)) {
		log_err("init evp tag failed");
		goto error;
	}

	if(1 != EVP_DecryptInit_ex(ctx, NULL, NULL, key, iv)) {
		log_err("init key and iv failed");
		goto error;
	}

	if(1 != EVP_DecryptUpdate(ctx, NULL, &len, NULL, (const int)nbytes_ciphertext)) {
		log_err("provide ciphertext length failed");
		goto error;
	}

	if(1 != EVP_DecryptUpdate(ctx, NULL, &len, aad, (const int)aad_len)) {
		log_err("provide aad failed");
		goto error;
	}

	if (1 != EVP_DecryptUpdate(ctx, plaintext, &len, ciphertext, (const int)nbytes_ciphertext)) {
		log_err("decryption failed");
		goto error;
	}

	EVP_CIPHER_CTX_free(ctx);

	*ret_nbytes_plaintext = len;

	return 0;

error:

	if (ctx) {
		EVP_CIPHER_CTX_free(ctx);
	}

	return -1;
}
*/

static int aes_cbc_encrypt(const EVP_CIPHER* type,
                           const byte_t* plaintext, const size_t nbytes_plaintext,
                           const byte_t key[], const byte_t iv[],
                           byte_t** ret_ciphertext, size_t* ret_nbytes_ciphertext)
{
	int err = 0;

	EVP_CIPHER_CTX ctx;
	const size_t max_nbytes_ciphertext = nbytes_plaintext + AES_BLOCK_SIZE;
	byte_t* ciphertext = NULL;
	int clen = 0;
	int flen = 0;

	if (!type) {
		err = EINVAL;
		log_err("cipher type not specified");
		goto error;
	}

	if (!plaintext) {
		err = EINVAL;
		log_err("plaintext should not be null");
		goto error;
	}

	EVP_CIPHER_CTX_init(&ctx);

	if (1 != EVP_EncryptInit_ex(&ctx, type, NULL, key, iv)) {
		log_err("init evp context for aes-128-cbc encryption failed");
		goto error;
	}

	if (!(ciphertext = (byte_t*)malloc(max_nbytes_ciphertext * sizeof(byte_t)))) {
		err = errno;
		log_err("alloc for ciphertex failed");
		goto error;
	}

	clen = (int)max_nbytes_ciphertext;
	if (1 != EVP_EncryptUpdate(&ctx, ciphertext, &clen, plaintext, (int)nbytes_plaintext)) {
		log_err("aes-128-cbc encryption failed");
		goto error;
	}

	if (1 != EVP_EncryptFinal_ex(&ctx, ciphertext+clen, &flen)) {
		log_err("final aes-128-cbc decryption failed");
		goto error;
	}

	*ret_ciphertext = ciphertext;
	*ret_nbytes_ciphertext = (size_t)clen + (size_t)flen;

	EVP_CIPHER_CTX_cleanup(&ctx);

	return 0;

error:

	EVP_CIPHER_CTX_cleanup(&ctx);

	if (ciphertext) {
		free(ciphertext);
	}

	errno = err;

	return -1;
}

static int aes_cbc_decrypt(const EVP_CIPHER* type,
                           const byte_t* ciphertext, const size_t nbytes_ciphertext,
	                       const byte_t key[], const byte_t iv[],
	                       byte_t** ret_plaintext, size_t* ret_nbytes_plaintext)
{
	int err = 0;

	EVP_CIPHER_CTX ctx;
	byte_t* plaintext = NULL;
	int plen = 0;
	int flen = 0;

	if (!type) {
		err = EINVAL;
		log_err("cipher type not specified");
		goto error;
	}

	if (!ciphertext) {
		err = EINVAL;
		log_err("ciphertext should not be null");
		goto error;
	}

	EVP_CIPHER_CTX_init(&ctx);

	if (1 != EVP_DecryptInit_ex(&ctx, type, NULL, key, iv)) {
		log_err("init evp context for aes-128-cbc failed");
		goto error;
	}

	if (!(plaintext = (byte_t*)malloc((nbytes_ciphertext + AES_BLOCK_SIZE) * sizeof(byte_t)))) {
		err = errno;
		log_err("alloc for plaintext failed");
		goto error;
	}

	plen = (int)(nbytes_ciphertext + AES_BLOCK_SIZE);
	if (1 != EVP_DecryptUpdate(&ctx, plaintext, &plen, ciphertext, (int)nbytes_ciphertext)) {
		log_err("aes-128-cbc decryption failed");
		goto error;
	}

	if (1 != EVP_DecryptFinal_ex(&ctx, plaintext+plen, &flen)) {
		log_err("final aes-128-cbc decryption failed");
		goto error;
	}

	*ret_plaintext = plaintext;
	*ret_nbytes_plaintext = (size_t)plen + (size_t)flen;

	EVP_CIPHER_CTX_cleanup(&ctx);

	return 0;

error:

	EVP_CIPHER_CTX_cleanup(&ctx);

	if (plaintext) {
		free(plaintext);
	}

	errno = err;

	return -1;
}

int aes128_cbc_encrypt(const byte_t* plaintext, const size_t nbytes_plaintext,
	                   const byte_t key[], const byte_t iv[],
	                   byte_t** ret_ciphertext, size_t* ret_nbytes_ciphertext)
{
	return aes_cbc_encrypt(EVP_aes_128_cbc(),
                           plaintext, nbytes_plaintext,
                           key, iv,
                           ret_ciphertext, ret_nbytes_ciphertext);
}



int aes128_cbc_decrypt(const byte_t* ciphertext, const size_t nbytes_ciphertext,
	                   const byte_t key[], const byte_t iv[],
	                   byte_t** ret_plaintext, size_t* ret_nbytes_plaintext)
{
	return aes_cbc_decrypt(EVP_aes_128_cbc(),
                           ciphertext, nbytes_ciphertext,
                           key, iv,
                           ret_plaintext, ret_nbytes_plaintext);
}

