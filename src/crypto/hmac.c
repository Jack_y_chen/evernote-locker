#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <errno.h>

#include <stdint.h>

#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/hmac.h>
#include <openssl/sha.h>
#include <netinet/in.h>

#include "log.h"
#include "crypto.h"


int hmac_sha256(const byte_t* msg, const size_t nbytes_msg,
				const byte_t* key, size_t nbytes_key,
				byte_t digest[])
{
	HMAC_CTX ctx;
	unsigned int len = 0;

	HMAC_CTX_init(&ctx);

	if (1 != HMAC_Init_ex(&ctx, key, (int)nbytes_key, EVP_sha256(), NULL)) {
		log_err("init hmac engine failed");
		goto error;
	}

	if (1 != HMAC_Update(&ctx, msg, nbytes_msg)) {
		log_err("perform hmac failed");
		goto error;
	}

	if (1 != HMAC_Final(&ctx, digest, &len)) {
		log_err("perform final hmac failed");
		goto error;
	}

	HMAC_CTX_cleanup(&ctx);

	return 0;

error:

	HMAC_CTX_cleanup(&ctx);

	return -1;
}

#if 0
int hmac_sha256(const byte_t* msg, const size_t nbytes_msg,
				const byte_t* key, size_t nbytes_key,
				byte_t digest[])
{
	int err = 0;

	byte_t k_ipad[65] = {0};   /* inner padding -
								 * key XORd with ipad
								 */
	byte_t k_opad[65] = {0};   /* outer padding -
								 * key XORd with opad
								 */
	byte_t tk[SHA256_DIGEST_LENGTH] = {0};
	byte_t tk2[SHA256_DIGEST_LENGTH] = {0};

	byte_t* in_buffer = NULL;
	byte_t* out_buffer = NULL;

	size_t i;

	/* if key is longer than 64 bytes reset it to key=sha256(key) */
	if ( nbytes_key > 64 ) {

		if (!SHA256(key, nbytes_key, tk)) {
			log_err("sha256 failed");
			goto error;
		}

		key = tk;
		nbytes_key = SHA256_DIGEST_LENGTH;
	}
	memcpy( k_ipad, key, nbytes_key );
	memcpy( k_opad, key, nbytes_key );

	/* XOR key with ipad and opad values */
	for (i = 0; i < 64; i++) {
		k_ipad[i] ^= 0x36;
		k_opad[i] ^= 0x5c;
	}

	/*
	 * perform inner SHA256
	 */

	if (!(in_buffer = (byte_t*)malloc((64 + nbytes_msg) * sizeof(byte_t)))) {
		err = errno;
		log_err("in buffer allocation failed");
		goto error;
	}
	memcpy(in_buffer, k_ipad, 64);
	memcpy(in_buffer + 64, msg, nbytes_msg);

	if (!SHA256(in_buffer, 64 + nbytes_msg, tk2)) {
		log_err("sha256 failed");
		goto error;
	}


	/*
	 * perform outer SHA256
	 */
	if (!(out_buffer = (byte_t*)malloc((64 + SHA256_DIGEST_LENGTH) * sizeof(byte_t)))) {
		err = errno;
		log_err("out buffer allocation failed");
		goto error;
	}
	memcpy(out_buffer, k_opad, 64);
	memcpy(out_buffer + 64, tk2, SHA256_DIGEST_LENGTH);

	if (!SHA256(out_buffer, 64 + SHA256_DIGEST_LENGTH, digest)) {
		log_err("sha256 failed");
		goto error;
	}

	free(out_buffer);
	free(in_buffer);

	return 0;

error:

	if (out_buffer) {
		free(out_buffer);
	}

	if (in_buffer) {
		free(in_buffer);
	}

	errno = err;

	return -1;
}
#endif
