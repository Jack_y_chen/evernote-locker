#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <errno.h>

#include <openssl/evp.h>

#include "log.h"
#include "crypto.h"


#if 0

#include <arpa/inet.h>

#include <stdint.h>

static void xor_bytes(byte_t b1[], const byte_t b2[], const size_t nbytes) {
	size_t i;
	for (i = 0; i < nbytes; ++i) {
		b1[i] ^= b2[i];
	}
}

static int prf(const char* password, const byte_t* msg, const size_t nbytes_msg, byte_t digest[]) {

	if (!password) {
		log_err("password should not be null");
		return -1;
	}

	if (!msg) {
		log_err("msg should not be null");
		return -1;
	}

	return hmac_sha256(msg, nbytes_msg, (const byte_t*)password, strlen(password), digest);
}

static int pbkdf2_f(const char* password,
                    const byte_t* salt, const size_t nbytes_salt,
                    const size_t iteration,
                    const uint32_t block_index,
                    byte_t digest[]) {

	int err = 0;
	const uint32_t block_index_be = htonl(block_index);
	byte_t* first_msg = NULL;

	byte_t u[SHA256_DIGEST_LENGTH] = {0};
	byte_t t[SHA256_DIGEST_LENGTH] = {0};
	uint32_t i;

	first_msg = (byte_t*)malloc((nbytes_salt + sizeof(uint32_t)) * sizeof(byte_t));
	if (!first_msg) {
		err = errno;
		log_err("mem allocation failed");
		goto error;
	}

	memcpy(first_msg, salt, nbytes_salt);
	memcpy(first_msg+nbytes_salt, &block_index_be, sizeof(uint32_t));
	
	if (prf(password, first_msg, nbytes_salt+sizeof(uint32_t), u)) {
		err = errno;
		log_err("prf generation failed");
		goto error;
	}

	memcpy(t, u, SHA256_DIGEST_LENGTH);

	for (i = 2; i <= iteration; ++i) {

		if (prf(password, u, sizeof(u), u)) {
			err = errno;
			log_err("prf generation failed");
			goto error;
		}

		xor_bytes(t, u, sizeof(t));
	}

	memcpy(digest, t, sizeof(t));

	free(first_msg);

	return 0;

error:

	if (first_msg) {
		free(first_msg);
	}

	return -1;
}

byte_t* pbkdf2_hmac_sha256(const char* password,
                         const byte_t* salt, const size_t nbytes_salt,
                         const size_t iteration,
                         const unsigned short keylen) {
	int err = 0;
	uint32_t num_blocks = 0;
	byte_t buffer[SHA256_DIGEST_LENGTH] = {0};
	uint32_t i;
	byte_t* output = NULL;

	if (!password) {
		err = EINVAL;
		log_err("password should not be null");
		goto error;
	}

	if (!salt) {
		err = EINVAL;
		log_err("salt should not be null");
		goto error;
	}

	num_blocks = keylen / SHA256_DIGEST_LENGTH;
	if (keylen % SHA256_DIGEST_LENGTH) {
		++num_blocks;
	}

	for (i = 1; i <= num_blocks; ++i) {
		pbkdf2_f(password, salt, nbytes_salt, iteration, i, buffer);
	}

	output = (byte_t*)malloc(keylen * sizeof(byte_t));
	if (!output) {
		err = errno;
		log_err("mem allocation failed");
		goto error;
	}

	memcpy(output, buffer, keylen);

	return output;

error:

	if (output) {
		free(output);
	}

	return NULL;
}
#endif

byte_t* pbkdf2_hmac_sha256(const char* password,
                         const byte_t* salt, const size_t nbytes_salt,
                         const size_t iteration,
                         const unsigned short keylen) {

	int err = 0;
	byte_t* digest = NULL;

	if (!password) {
		err = EINVAL;
		log_err("password should not be null");
		goto error;
	}

	if (!salt) {
		err = EINVAL;
		log_err("salt should not be null");
		goto error;
	}

	if (!(digest = (byte_t*)malloc((size_t)keylen * sizeof(byte_t)))) {
		err = errno;
		log_err("alloc for digest failed");
		goto error;
	}

	if (1 != PKCS5_PBKDF2_HMAC(password, (int)strlen(password), salt, (int)nbytes_salt, (int)iteration, EVP_sha256(), keylen, digest)) {
		log_err("digest failed");
		goto error;
	}

	return digest;

error:

	if (digest) {
		free(digest);
	}

	errno = err;

	return NULL;
}

