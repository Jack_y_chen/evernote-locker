#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <errno.h>

#include <openssl/bio.h>
#include <openssl/evp.h>
#include <openssl/buffer.h>

#include "log.h"
#include "crypto.h"

/*
static int calc_b64_len(const char* b64_str, size_t* ret_len) {

	const size_t len = strlen(b64_str);
	size_t padding = 0;

	if (!b64_str || !len) {
		log_err("b64_str and len should not be null");
		return -1;
	}

	if (len >= 2 && b64_str[len-1UL] == '=' && b64_str[len-2UL] == '=') {
		padding = 2;
	}
	else if (len >= 1 && b64_str[len-1UL] == '=') {
		padding = 2;
	}
	else {
		padding = 0;
	}

	*ret_len = (int)((double)len * 0.75) - padding;

	return 0;
}
*/

int base64_decode(const char* b64_str, byte_t** ret_buf, size_t* ret_buf_len) {

	int err = 0;
	BIO* b64 = NULL;
	BIO* bmem = NULL;
	size_t raw_length = 0;
	byte_t* buffer = NULL;

	if (!b64_str) {
		err = EINVAL;
		log_err("b64 string should not be null");
		goto error;
	}
/*
	err = calc_b64_len(b64_str, &raw_length);
	if (err) {
		log_err("calc base64 raw length failed");
		goto error;
	}
*/
	raw_length = strlen(b64_str);

	buffer = (byte_t*)malloc(raw_length * sizeof(byte_t));
	if (!buffer) {
		err = errno;
		log_err("raw buffer allocation failed");
		goto error;
	}

	b64 = BIO_new(BIO_f_base64());
	if (!b64) {
		log_err("alloc b64 decoder failed");
		goto error;
	}

	BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);

	bmem = BIO_push(b64, BIO_new_mem_buf((char*)b64_str, (int)strlen(b64_str)));
	if (!bmem) {
		log_err("alloc b64 container failed");
		goto error;
	}
/*
	if (BIO_read(bmem, buffer, raw_length) != raw_length) {
		log_err("b64 decode failed: raw length invalid: %lu", raw_length);
		goto error;
	}
*/
	raw_length = (size_t)BIO_read(bmem, buffer, (int)raw_length);

	*ret_buf_len = raw_length;
	*ret_buf = buffer;

	BIO_free_all(bmem);

	return 0;

error:

	if (buffer) {
		free(buffer);
	}

	if (bmem) {
		BIO_free_all(bmem);
	}

	errno = err;

	return -1;
}

char* base64_encode(const byte_t* data, const size_t nbytes_data) {

	int err = 0;
	BIO* b64 = NULL;
	BIO* bmem = NULL;
	BUF_MEM* bptr = NULL;
	char* ret = NULL;

	if (!data) {
		err = EINVAL;
		log_err("b64 raw data should not be null");
		goto error;
	}

	b64 = BIO_new(BIO_f_base64());
	if (!b64) {
		log_err("alloc b64 decoder failed");
		goto error;
	}

	BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);

	bmem = BIO_new(BIO_s_mem());
	if (!bmem) {
		log_err("alloc b64 container failed");
		goto error;
	}

	b64 = BIO_push(b64, bmem);
	BIO_write(b64, (const void*)data, (int)nbytes_data);
	BIO_flush(b64);
	BIO_get_mem_ptr(b64, &bptr);

	ret = (char*)malloc((size_t)(bptr->length+1) * sizeof(char));
	if (!ret) {
		err = errno;
		log_err("raw buffer allocation failed");
		goto error;
	}

	memcpy(ret, bptr->data, bptr->length);
	ret[bptr->length] = '\0';

	BIO_free_all(b64);

	return ret;

error:

	if (ret) {
		free(ret);
	}

	if (b64) {
		BIO_free_all(b64);
	}

	errno = err;

	return NULL;
}

