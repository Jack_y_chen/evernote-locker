define check_non_empty
$(if $(strip $(1)),$(strip $(1)),$(error cannot be empty))
endef

define find_cmd
$(if $(filter 0,$(shell which $(call check_non_empty,$(1)); echo $$?)),$(shell which $(strip $(1))),$(error no such command:[$(strip $(1)])))
endef

define check_one_of
$(if $(filter $(strip $(2)),$(strip $(1))),$(strip $(1)),$(error $(strip $(1)) not in [$(strip $(2))]))
endef

CMD_CP      := $(call find_cmd, cp)
CMD_ECHO    := $(call find_cmd, echo)
CMD_FIND    := $(call find_cmd, find)
CMD_MKDIR   := $(call find_cmd, mkdir)
CMD_RM      := $(call find_cmd, rm)
CMD_PYTHON  := $(call find_cmd, python)
CMD_SED     := $(call find_cmd, sed)
CMD_UNAME   := $(call find_cmd, uname)
CMD_INSTALL := $(call find_cmd, install)


build       := release
BUILD       := $(call check_one_of, $(build), debug release)

host        := $(shell $(CMD_UNAME))
HOST        := $(call check_one_of, $(host), Linux Darwin)

LIBNAME     := tmenc
OUTPUT      := lib$(LIBNAME).so


CC := /usr/bin/gcc
CXX := /usr/bin/g++
LD := /usr/bin/ld
AR := /usr/bin/ar
AS := /usr/bin/as
NM := /usr/bin/nm
RANLIB :=/usr/bin/ranlib
STRIP := /usr/bin/strip

ifeq ($(HOST), Darwin)
	#SYSROOT := /Applications/Xcode.app/Contents/Developer/Platforms/MacOSX.platform/Developer/SDKs/MacOSX10.9.sdk
	SYSROOT := /Developer/SDKs/MacOSX10.6.sdk
else
	SYSROOT := /
endif

ifeq ($(BUILD), debug)
	OPTIMIZE := -O1
	DEBUGMACRO := -D_DEBUG -g
else
	OPTIMIZE := -O3
	DEBUGMACRO := -D_RELEASE
endif

CFLAGS      := -pedantic -std=c89 -ansi \
               $(DEBUGMACRO) $(OPTIMIZE) -fPIC \
               -Wall -Wpointer-arith -Wcast-align \
               -Wwrite-strings -Wnested-externs \
               -Winline -Wredundant-decls \
               -Wextra -Wconversion -Wmissing-prototypes \
               -Wstrict-prototypes -Wsign-compare \
               -Wno-deprecated \
               -pthread \
               -I$(SYSROOT)/usr/include/libxml2 \
               -I$(CURDIR)/include

SONAME_SPECIFIER := $(if $(filter Darwin,$(HOST)),dylib_install_name,soname)

LDFLAGS     := -pthread -L$(SYSROOT)/usr/lib -lxml2 -lcrypto


OBJS        := $(patsubst %.c, %.o, $(shell $(CMD_FIND) src -type f -name "*.c" -print | $(CMD_SED) 's/ /\\ /g'))


.PHONY: all clean


all: $(OUTPUT)


$(OUTPUT): $(OBJS)
	"$(CC)" -shared $(OBJS) $(LDFLAGS) -Wl,-$(SONAME_SPECIFIER),$(OUTPUT) -o "$(OUTPUT)"

%.o: %.c
	"$(CC)" $(CFLAGS) -c $< -o $@

test: $(OUTPUT) test/lock.o test/unlock.o
	"$(CC)" test/lock.o -L. -l$(LIBNAME) $(LDFLAGS) -o test/lock
	"$(CC)" test/unlock.o -L. -l$(LIBNAME) $(LDFLAGS) -o test/unlock
	"$(CMD_CP)" "$(OUTPUT)" test/

clean:
	"$(CMD_RM)" -f test/*.so test/*.o test/lock test/unlock
	"$(CMD_RM)" -f $(OUTPUT)
	"$(CMD_RM)" -f $(OBJS)

