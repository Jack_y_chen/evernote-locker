#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <errno.h>

#include "evernote.h"

#define BUFFER_SIZE 4UL * 1024UL * 1024UL

int main(int argc, char* argv[]) {

	int err = 0;

	const char* password = NULL;
	int is_web = 0;

	byte_t* buffer = NULL;
	size_t nbytes_read = 0UL;

	char* locked_note = NULL;

	if (argc < 4) {
		fprintf(stderr, "usage: lock [-pw PASSWORD] [-web|-note]\n");
		goto error;
	}

	password = argv[2];
	is_web = !strcmp(argv[3], "-web");

	if (!(buffer = (byte_t*)malloc(BUFFER_SIZE * sizeof(byte_t)))) {
		err = E2BIG;
		fprintf(stderr, "out of memory: \n");
		goto error;
	}

	nbytes_read = fread(buffer, 1UL, BUFFER_SIZE, stdin);

	if (nbytes_read <= 0UL) {
		fprintf(stderr, "read failed: %s\n", strerror(errno));
		goto error;
	}

	if (nbytes_read >= BUFFER_SIZE) {
		err = E2BIG;
		fprintf(stderr, "out of memory: \n");
		goto error;
	}

	buffer[nbytes_read] = '\0';

	locked_note = evernote_lock_note(password, (const char*)buffer, is_web);
	if (!locked_note) {
		fprintf(stderr, "lock note failed\n");
		goto error;
	}

	fprintf(stdout, "%s", locked_note);

	free(locked_note);
	free(buffer);

	return 0;

error:

	if (locked_note) {
		free(locked_note);
	}

	if (buffer) {
		free(buffer);
	}

	return err;
}
