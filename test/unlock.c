#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <errno.h>

#include "evernote.h"

#define BUFFER_SIZE 4UL * 1024UL * 1024UL


int main(int argc, char* argv[]) {

	int err = 0;

	const char* password = NULL;
	int is_web = 0;

	byte_t* buffer = NULL;
	size_t nbytes_read = 0UL;

	char* unlocked_note = NULL;

	if (argc < 4) {
		fprintf(stderr, "usage: unlock [-password PASSWORD] [-web|-note]\n");
		fprintf(stderr, "no password provided\n");
		goto error;
	}

	password = argv[2];
	is_web = !strcmp(argv[3], "-web");

	if (!(buffer = (byte_t*)malloc(BUFFER_SIZE * sizeof(byte_t)))) {
		err = E2BIG;
		fprintf(stderr, "out of memory: \n");
		goto error;
	}

	nbytes_read = fread(buffer, 1UL, BUFFER_SIZE, stdin);

	if (nbytes_read <= 0UL) {
		fprintf(stderr, "read failed: %s\n", strerror(errno));
		goto error;
	}

	if (nbytes_read >= BUFFER_SIZE) {
		err = E2BIG;
		fprintf(stderr, "out of memory: \n");
		goto error;
	}

	buffer[nbytes_read] = '\0';

	unlocked_note = evernote_unlock_note(password, (const char*)buffer, is_web);
	if (!unlocked_note) {
		fprintf(stderr, "unlock note failed\n");
		goto error;
	}

	fprintf(stdout, "%s", unlocked_note);

	free(unlocked_note);
	free(buffer);

	return 0;

error:

	if (unlocked_note) {
		free(unlocked_note);
	}

	if (buffer) {
		free(buffer);
	}

	return err;
}
